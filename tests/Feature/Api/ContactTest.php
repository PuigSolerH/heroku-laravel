<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ContactTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_a_success_message()
    {
        $data = [
            'data' => [
                'email' => $this->user->email,
                'subject' => 'secret',
                'comment' => 'fffffffffffffffffffff',
            ]
        ];

        $response = $this->postJson('/api/contact', $data);

        $response->assertStatus(200)
            ->assertJson([
                "message" => "Request completed"
            ]);
    }

    /** @test */
    public function it_returns_field_required_validation_errors_on_invalid_contact_form()
    {
        $data = [];

        $response = $this->postJson('/api/contact', $data);

        $response->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'email' => ['field is required.'],
                    'subject' => ['field is required.'],
                    'comment' => ['field is required.'],
                ]
            ]);
    }
}
