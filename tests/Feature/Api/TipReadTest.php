<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TipReadTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_an_empty_array_of_tips_when_no_tips_exist()
    {
        $response = $this->getJson('/api/tips');

        $response->assertStatus(200)
            ->assertJson([
                'tips' => [],
                'tipsCount' => 0
            ]);
    }

    /** @test */
    public function it_returns_the_tips_and_correct_total_tip_count()
    {
        $categories = factory(\App\Category::class)->times(2)->create();

        $tips = $this->user->tip()->saveMany(factory(\App\Tip::class)->times(2)->make());

        $response = $this->getJson('/api/tips');

        $response->assertStatus(200)
            ->assertJson([
                'tips' => [
                    [
                        'slug' => $tips[1]->slug,
                        'image' => 'https://cdn.worldvectorlogo.com/logos/laravel.svg',
                        'title' => $tips[1]->title,
                        'header' => $tips[1]->header,
                        'body' => $tips[1]->body,
                        'category' => $tips[1]->categoryList,
                        'createdAt' => $tips[1]->created_at->toAtomString(),
                        'author' => [
                            'username' => $this->user->username,
                            'bio' => $this->user->bio,
                            'image' => $this->user->image,
                        ]
                    ],
                    [
                        'slug' => $tips[0]->slug,
                        'image' => 'https://cdn.worldvectorlogo.com/logos/laravel.svg',
                        'title' => $tips[0]->title,
                        'header' => $tips[0]->header,
                        'body' => $tips[0]->body,
                        'category' => $tips[0]->categoryList,
                        'createdAt' => $tips[0]->created_at->toAtomString(),
                        'author' => [
                            'username' => $this->user->username,
                            'bio' => $this->user->bio,
                            'image' => $this->user->image,
                        ]
                    ],
                ],
                'tipsCount' => 2
            ]);
    }

    /** @test 
     * 
     * ACTUALLY IT DOESN'T WORK.
     * 
     */
    public function it_returns_the_tip_by_slug_if_valid_and_not_found_error_if_invalid()
    {
        $category = factory(\App\Category::class)->times(1)->create();

        $tip = $this->loggedInUser->tip()->save(factory(\App\Tip::class)->make());

        $response = $this->getJson("/api/tips/{$tip->slug}");

        $response->assertStatus(200)
            ->assertJson([
                'tip' => [
                    'slug' => $tip->slug,
                    'image' => 'https://cdn.worldvectorlogo.com/logos/laravel.svg',
                    'title' => $tip->title,
                    'header' => $tip->header,
                    'body' => $tip->body,
                    'category' => $tip->categoryList,
                    'createdAt' => $tip->created_at->toAtomString(),
                    'author' => [
                        'username' => $this->user->username,
                        'bio' => $this->user->bio,
                        'image' => $this->user->image,
                    ]
                ]
            ]);

        $response = $this->getJson('/api/tips/randominvalidslug');

        $response->assertStatus(404);
    }
}
