<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class OperationCreateTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_the_operation_on_successfully_creating_a_new_operation()
    {
        $user = factory(\App\User::class)->times(1)->create();

        $teacher = factory(\App\Teacher::class)->times(1)->create();

        $classroom = factory(\App\Classroom::class)->times(1)->create();

        $student = \App\Student::create([
            'user_id' => $this->loggedInUser->id,
            'classroom_id' => $classroom[0]->id,
        ]);

        $category = factory(\App\Category::class)->times(1)->create();

        $data = [
            'operation' => [
                'category' => $category[0]->name,
                'quantity' => '4',
                'student_id' => $student->id,
            ]
        ];

        $response = $this->postJson('/api/operations', $data, $this->headers);

        $response->assertStatus(200)
            ->assertJson([
                'operation' => [
                    'id' => $response->baseResponse->original['operation']['id'],
                    'category' => $category[0]->name,
                    'quantity' => '4',
                    'author' => [
                        'username' => $this->loggedInUser->username,
                        'image' => $this->loggedInUser->image,
                        'classroom' => $classroom[0]->name
                    ]
                ]
            ]);
    }

    /** @test */
    public function it_returns_appropriate_field_validation_errors_when_creating_a_new_operation_with_invalid_inputs()
    {
        $data = [
            'operation' => [
                'category' => '',
                'quantity' => '',
            ]
        ];

        $response = $this->postJson('/api/operations', $data, $this->headers);

        $response->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'category' => ['field is required.'],
                    'quantity' => ['field is required.'],
                ]
            ]);
    }

    /** @test */
    public function it_returns_an_unauthorized_error_when_trying_to_add_operation_without_logging_in()
    {
        $response = $this->postJson('/api/operations', []);

        $response->assertStatus(401);
    }
}
