<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TipFilterTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_an_empty_array_of_tips_when_no_tips_exist_with_the_category_or_an_invalid_category()
    {
        $response = $this->getJson('/api/tips?category=test');

        $response->assertStatus(200)
            ->assertJson([
                'tips' => [],
                'tipsCount' => 0
            ]);

        $response = $this->getJson('/api/tips?category=somerandomcategory');

        $response->assertStatus(200)
            ->assertJson([
                'tips' => [],
                'tipsCount' => 0
            ]);
    }

    /** @test */
    public function it_returns_the_tips_with_the_category_along_with_correct_total_tip_count()
    {
        $categories = factory(\App\Category::class)->times(1)->create();

        $tips = $this->user->tip()->saveMany(factory(\App\Tip::class)->times(3)->make());

        $response = $this->getJson("/api/tips?category={$categories[0]->name}");

        $response->assertStatus(200)
            ->assertJson([
                'tips' => [
                    [
                        'slug' => $tips[2]->slug,
                        'image' => 'https://cdn.worldvectorlogo.com/logos/laravel.svg',
                        'title' => $tips[2]->title,
                        'header' => $tips[2]->header,
                        'body' => $tips[2]->body,
                        'category' => $tips[2]->categoryList,
                        'createdAt' => $tips[2]->created_at->toAtomString(),
                        'author' => [
                            'username' => $this->user->username,
                            'bio' => $this->user->bio,
                            'image' => $this->user->image,
                        ]
                    ],
                    [
                        'slug' => $tips[1]->slug,
                        'image' => 'https://cdn.worldvectorlogo.com/logos/laravel.svg',
                        'title' => $tips[1]->title,
                        'header' => $tips[1]->header,
                        'body' => $tips[1]->body,
                        'category' => $tips[1]->categoryList,
                        'createdAt' => $tips[1]->created_at->toAtomString(),
                        'author' => [
                            'username' => $this->user->username,
                            'bio' => $this->user->bio,
                            'image' => $this->user->image,
                        ]
                    ],
                    [
                        'slug' => $tips[0]->slug,
                        'image' => 'https://cdn.worldvectorlogo.com/logos/laravel.svg',
                        'title' => $tips[0]->title,
                        'header' => $tips[0]->header,
                        'body' => $tips[0]->body,
                        'category' => $tips[0]->categoryList,
                        'createdAt' => $tips[0]->created_at->toAtomString(),
                        'author' => [
                            'username' => $this->user->username,
                            'bio' => $this->user->bio,
                            'image' => $this->user->image,
                        ]
                    ]
                ],
                'tipsCount' => 3
            ]);
    }

    /** @test */
    public function it_returns_an_empty_array_of_tips_when_no_tips_exist_by_the_author_or_invalid_author()
    {
        $response = $this->getJson('/api/tips?author=test');

        $response->assertStatus(200)
            ->assertJson([
                'tips' => [],
                'tipsCount' => 0
            ]);

        $response = $this->getJson('/api/tips?author=somerandomcategory');

        $response->assertStatus(200)
            ->assertJson([
                'tips' => [],
                'tipsCount' => 0
            ]);
    }

    /** @test */
    public function it_returns_the_tips_by_the_author_along_with_correct_total_tip_count()
    {
        $categories = factory(\App\Category::class)->times(1)->create();

        $tips = $this->user->tip()->saveMany(factory(\App\Tip::class)->times(3)->make());
        $this->loggedInUser->tip()->saveMany(factory(\App\Tip::class)->times(5)->make());

        $response = $this->getJson("/api/tips?author={$this->user->username}");

        $response->assertStatus(200)
            ->assertJson([
                'tips' => [
                    [
                        'slug' => $tips[2]->slug,
                        'title' => $tips[2]->title,
                        'author' => [
                            'username' => $this->user->username
                        ]
                    ],
                    [
                        'slug' => $tips[1]->slug,
                        'title' => $tips[1]->title,
                        'author' => [
                            'username' => $this->user->username
                        ]
                    ],
                    [
                        'slug' => $tips[0]->slug,
                        'title' => $tips[0]->title,
                        'author' => [
                            'username' => $this->user->username
                        ]
                    ],
                ],
                'tipsCount' => 3
            ]);
    }
}
