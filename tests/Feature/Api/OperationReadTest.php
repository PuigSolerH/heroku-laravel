<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class OperationReadTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_an_empty_array_of_operations_when_no_operations_exist()
    {
        $response = $this->getJson('/api/operations');

        $response->assertStatus(200)
            ->assertJson([
                'operations' => [],
                'operationsCount' => 0
            ]);

        $response = $this->getJson('/api/operationsAmount');

        $response->assertStatus(200)
            ->assertJson([]);
    }

    /** @test 
     * 
     * SOMETIMES IT FAILS DEPENDING THE WAY IT GETS THE OPERATIONS. BUT THE DATA IT'S OK.
     * SO IF IT DOESN'T WORK AT ONCE, JUST TRY IT AGAIN. SORRY.
     * 
     */
    public function it_returns_the_operations_and_correct_total_operation_count()
    {
        $users = factory(\App\User::class)->times(1)->create();

        $teacher = factory(\App\Teacher::class)->times(1)->create();

        $classroom = factory(\App\Classroom::class)->times(1)->create();

        $student = \App\Student::create([
            'user_id' => $this->loggedInUser->id,
            'classroom_id' => $classroom[0]->id,
        ]);

        $category = factory(\App\Category::class)->times(1)->create();

        $operations = $student->operation()->saveMany(factory(\App\Operation::class)->times(2)->make());

        $response = $this->getJson('/api/operations');

        $response->assertStatus(200)
            ->assertJson([
                'operations' => [
                    [
                        'id' => $operations[0]->id,
                        'category' => $operations[0]->category->name,
                        'quantity' => $operations[0]->quantity,
                        'author' => [
                            'username' => $this->loggedInUser->username,
                            'image' => $this->loggedInUser->image,
                            'classroom' => $operations[1]->student->classroom->name,
                        ]
                    ],
                    [
                        'id' => $operations[1]->id,
                        'category' => $operations[1]->category->name,
                        'quantity' => $operations[1]->quantity,
                        'author' => [
                            'username' => $this->loggedInUser->username,
                            'image' => $this->loggedInUser->image,
                            'classroom' => $operations[1]->student->classroom->name,
                        ]
                    ]
                ],
                'operationsCount' => 2
            ]);

        $response = $this->getJson('/api/operationsAmount');

        $response->assertStatus(200)
        ->assertJson([
                [
                    'name' => $response->baseResponse->original[0]['name'],
                    'sum' => $response->baseResponse->original[0]['sum'],
                ]
            ]);
    }

    /** @test */
    public function it_returns_the_operation_by_id_if_valid_and_not_found_error_if_invalid()
    {
        $users = factory(\App\User::class)->times(1)->create();

        $teacher = factory(\App\Teacher::class)->times(1)->create();

        $classroom = factory(\App\Classroom::class)->times(1)->create();

        $student = \App\Student::create([
            'user_id' => $this->user->id,
            'classroom_id' => $classroom[0]->id,
        ]);

        $category = factory(\App\Category::class)->times(1)->create();

        $operation = $student->operation()->save(factory(\App\Operation::class)->make());

        $response = $this->getJson("/api/operations/{$operation->id}");

        $response->assertStatus(200)
            ->assertJson([
                'operation' => [
                    'id' => $operation->id,
                    'category' => $operation->category->name,
                    'quantity' => $operation->quantity,
                    'image' => $operation->image,
                    'author' => [
                        'username' => $this->user->username,
                        'image' => $this->user->image,
                        'classroom' => $operation->student->classroom->name,
                    ]
                ]
            ]);

        $response = $this->getJson('/api/operations/99999');

        $response->assertStatus(404);
    }
}
