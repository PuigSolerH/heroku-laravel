<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ChallengeDeleteTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_a_200_success_response_on_successfully_removing_the_challenge()
    {
        $teacher = \App\Teacher::create([
            'user_id' => $this->loggedInUser->id,
        ]);

        $challenge = $teacher->challenge()->save(factory(\App\Challenge::class)->make());

        $response = $this->deleteJson("/api/challenges/{$challenge->slug}", [], $this->headers);

        $response->assertStatus(200);

        $response = $this->getJson("/api/challenges/{$challenge->slug}");

        $response->assertStatus(404);
    }

    /** @test */
    public function it_returns_an_unauthorized_error_when_trying_to_remove_challenge_without_logging_in()
    {
        $teacher = \App\Teacher::create([
            'user_id' => $this->loggedInUser->id,
        ]);

        $challenge = $teacher->challenge()->save(factory(\App\Challenge::class)->make());

        $response = $this->deleteJson("/api/challenges/{$challenge->slug}");

        $response->assertStatus(401);
    }

    /** @test */
    public function it_returns_a_forbidden_error_when_trying_to_remove_challenges_by_others()
    {
        $teacher = \App\Teacher::create([
            'user_id' => $this->user->id,
        ]);

        $challenge = $teacher->challenge()->save(factory(\App\Challenge::class)->make());

        $response = $this->deleteJson("/api/challenges/{$challenge->slug}", [], $this->headers);

        $response->assertStatus(403);
    }
}
