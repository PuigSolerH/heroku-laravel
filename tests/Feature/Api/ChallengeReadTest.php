<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ChallengeReadTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_an_empty_array_of_challenges_when_no_challenges_exist()
    {
        $response = $this->getJson('/api/challenges');

        $response->assertStatus(200)
            ->assertJson([
                'challenges' => [],
                'challengesCount' => 0
            ]);
    }

    /** @test 
     * 
     * SOMETIMES IT FAILS DEPENDING THE WAY IT GETS THE CHALLENGES. BUT THE DATA IT'S OK.
     * SO IF IT DOESN'T WORK AT ONCE, JUST TRY IT AGAIN. SORRY.
     * 
     */
    public function it_returns_the_challenges_and_correct_total_challenge_count()
    {
        $teacher = \App\Teacher::create([
            'user_id' => $this->user->id,
        ]);

        $challenges = $teacher->challenge()->saveMany(factory(\App\Challenge::class)->times(2)->make());

        $response = $this->getJson('/api/challenges');

        $response->assertStatus(200)
            ->assertJson([
                'challenges' => [
                    [
                        'slug' => $challenges[0]->slug,
                        'title' => $challenges[0]->title,
                        'description' => $challenges[0]->description,
                        'createdAt' => $challenges[0]->created_at->toAtomString(),
                        'author' => [
                            'username' => $this->user->username,
                            'image' => $this->user->image,
                            'rol' => 'teacher',
                            'classroom' => ""
                        ]
                    ],
                    [
                        'slug' => $challenges[1]->slug,
                        'title' => $challenges[1]->title,
                        'description' => $challenges[1]->description,
                        'createdAt' => $challenges[1]->created_at->toAtomString(),
                        'author' => [
                            'username' => $this->user->username,
                            'image' => $this->user->image,
                            'rol' => 'teacher',
                            'classroom' => ""
                        ]
                    ]
                ],
                'challengesCount' => 2
            ]);
    }

    /** @test */
    public function it_returns_the_challenge_by_slug_if_valid_and_not_found_error_if_invalid()
    {
        $teacher = \App\Teacher::create([
            'user_id' => $this->user->id,
        ]);

        $challenge = $teacher->challenge()->save(factory(\App\Challenge::class)->make());

        $response = $this->getJson("/api/challenges/{$challenge->slug}");

        $response->assertStatus(200)
            ->assertJson([
                'challenge' => [
                    'slug' => $challenge->slug,
                    'title' => $challenge->title,
                    'description' => $challenge->description,
                    'createdAt' => $challenge->created_at->toAtomString(),
                    'author' => [
                        'username' => $this->user->username,
                        'image' => $this->user->image,
                        'rol' => 'teacher',
                        'classroom' => ""
                    ]
                ]
            ]);

        $response = $this->getJson('/api/challenges/randominvalidslug');

        $response->assertStatus(404);
    }
}
