<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class OperationDeleteTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_a_200_success_response_on_successfully_removing_the_operation()
    {
        $users = factory(\App\User::class)->times(1)->create();

        $teacher = factory(\App\Teacher::class)->times(1)->create();

        $classroom = factory(\App\Classroom::class)->times(1)->create();

        $student = \App\Student::create([
            'user_id' => $this->loggedInUser->id,
            'classroom_id' => $classroom[0]->id,
        ]);

        $category = factory(\App\Category::class)->times(1)->create();

        $operation = $student->operation()->save(factory(\App\Operation::class)->make());

        $response = $this->deleteJson("/api/operations/{$operation->id}", [], $this->headers);

        $response->assertStatus(200);

        $response = $this->getJson("/api/operations/{$operation->id}");

        $response->assertStatus(404);
    }

    /** @test */
    public function it_returns_an_unauthorized_error_when_trying_to_remove_operation_without_logging_in()
    {
        $users = factory(\App\User::class)->times(1)->create();

        $teacher = factory(\App\Teacher::class)->times(1)->create();

        $classroom = factory(\App\Classroom::class)->times(1)->create();

        $student = \App\Student::create([
            'user_id' => $this->loggedInUser->id,
            'classroom_id' => $classroom[0]->id,
        ]);

        $category = factory(\App\Category::class)->times(1)->create();

        $operation = $student->operation()->save(factory(\App\Operation::class)->make());

        $response = $this->deleteJson("/api/operations/{$operation->id}");

        $response->assertStatus(401);
    }

    /** @test */
    public function it_returns_a_forbidden_error_when_trying_to_remove_operations_by_others()
    {
        $users = factory(\App\User::class)->times(1)->create();

        $teacher = factory(\App\Teacher::class)->times(1)->create();

        $classroom = factory(\App\Classroom::class)->times(1)->create();

        $student = \App\Student::create([
            'user_id' => $this->user->id,
            'classroom_id' => $classroom[0]->id,
        ]);

        $category = factory(\App\Category::class)->times(1)->create();

        $operation = $student->operation()->save(factory(\App\Operation::class)->make());

        $response = $this->deleteJson("/api/operations/{$operation->id}", [], $this->headers);

        $response->assertStatus(403);
    }
}
