<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CategoryTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_an_array_of_categories()
    {
        $categories = factory(\App\Category::class)->times(5)->create();

        $response = $this->getJson('/api/categories');

        $response->assertStatus(200)
            ->assertJson([
                'categories' => $categories->pluck('name')->toArray()
            ]);
    }

    /** @test */
    public function it_returns_an_empty_array_of_categories_when_there_are_none_in_database()
    {
        $response = $this->getJson('/api/categories');

        $response->assertStatus(200)
            ->assertJson([
                'categories' => []
            ]);

        $this->assertEmpty($response->json()['categories'], 'Expected empty categories array');
    }
}
