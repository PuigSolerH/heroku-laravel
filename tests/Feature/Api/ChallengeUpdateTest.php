<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ChallengeUpdateTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_the_updated_challenge_on_successfully_updating_the_challenge()
    {
        $teacher = \App\Teacher::create([
            'user_id' => $this->loggedInUser->id,
        ]);

        $challenge = $teacher->challenge()->save(factory(\App\Challenge::class)->make());

        $data = [
            'challenge' => [
                'title' => 'new title',
                'description' => 'new description',
            ]
        ];

        $response = $this->putJson("/api/challenges/{$challenge->slug}", $data, $this->headers);

        $response->assertStatus(200)
            ->assertJson([
                'challenge' => [
                    'slug' => 'new-title',
                    'title' => 'new title',
                    'description' => 'new description',
                    'createdAt' => $response->baseResponse->original['challenge']['createdAt'],
                    'author' => [
                        'username' => $this->loggedInUser->username,
                        'image' => $this->loggedInUser->image,
                        'rol' => 'teacher',
                        'classroom' => ""
                    ]
                ]
            ]);
    }

    /** @test */
    public function it_returns_appropriate_field_validation_errors_when_updating_the_challenge_with_invalid_inputs()
    {
        $teacher = \App\Teacher::create([
            'user_id' => $this->loggedInUser->id,
        ]);

        $challenge = $teacher->challenge()->save(factory(\App\Challenge::class)->make());

        $data = [
            'challenge' => [
                'title' => '',
                'description' => '',
            ]
        ];

        $response = $this->putJson("/api/challenges/{$challenge->slug}", $data, $this->headers);

        $response->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'title' => ['must be a string.'],
                    'description' => ['must be a string.'],
                ]
            ]);
    }

    /** @test */
    public function it_returns_an_unauthorized_error_when_trying_to_update_challenge_without_logging_in()
    {
        $teacher = \App\Teacher::create([
            'user_id' => $this->loggedInUser->id,
        ]);

        $challenge = $teacher->challenge()->save(factory(\App\Challenge::class)->make());

        $data = [
            'challenge' => [
                'title' => 'new title',
                'description' => 'new description',
                'body' => 'new body with random text',
            ]
        ];

        $response = $this->putJson("/api/challenges/{$challenge->slug}", $data);

        $response->assertStatus(401);
    }

    /** @test */
    public function it_returns_a_forbidden_error_when_trying_to_update_challenges_by_others()
    {
        $teacher = \App\Teacher::create([
            'user_id' => $this->user->id,
        ]);

        $challenge = $teacher->challenge()->save(factory(\App\Challenge::class)->make());

        $data = [
            'challenge' => [
                'title' => 'new title',
                'description' => 'new description',
                'body' => 'new body with random text',
            ]
        ];

        $response = $this->putJson("/api/challenges/{$challenge->slug}", $data, $this->headers);

        $response->assertStatus(403);
    }
}
