<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class OperationFilterTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_an_empty_array_of_operations_when_no_operations_exist_by_the_author_or_invalid_category()
    {
        $response = $this->getJson('/api/operations?student=test');

        $response->assertStatus(200)
            ->assertJson([
                'operations' => [],
                'operationsCount' => 0
            ]);

        $response = $this->getJson('/api/operations?category=somerandomcategory');

        $response->assertStatus(200)
            ->assertJson([
                'operations' => [],
                'operationsCount' => 0
            ]);
    }

    /** @test */
    public function it_returns_the_operations_by_the_author_along_with_correct_total_operations_count()
    {
        $users = factory(\App\User::class)->times(1)->create();

        $teacher = factory(\App\Teacher::class)->times(1)->create();

        $classroom = factory(\App\Classroom::class)->times(1)->create();

        $student = \App\Student::create([
            'user_id' => $this->loggedInUser->id,
            'classroom_id' => $classroom[0]->id,
        ]);

        $category = factory(\App\Category::class)->times(1)->create();

        $operations = $student->operation()->saveMany(factory(\App\Operation::class)->times(2)->make());

        $response = $this->getJson("/api/operations?author={$this->user->username}");

        $response->assertStatus(200)
            ->assertJson([
                'operations' => [
                    [
                        'id' => $operations[0]->id,
                        'category' => $operations[0]->category->name,
                        'quantity' => $operations[0]->quantity,
                        'image' => $operations[0]->image,
                        'author' => [
                            'username' => $this->loggedInUser->username,
                            'image' => $this->loggedInUser->image,
                            'classroom' => $operations[1]->student->classroom->name,
                        ]
                    ],
                    [
                        'id' => $operations[1]->id,
                        'category' => $operations[1]->category->name,
                        'quantity' => $operations[1]->quantity,
                        'image' => $operations[1]->image,
                        'author' => [
                            'username' => $this->loggedInUser->username,
                            'image' => $this->loggedInUser->image,
                            'classroom' => $operations[1]->student->classroom->name,
                        ]
                    ]
                ],
                'operationsCount' => 2
            ]);
    }

    /** @test */
    public function it_returns_the_operations_by_the_category_along_with_correct_total_operations_count()
    {
        $users = factory(\App\User::class)->times(1)->create();

        $teacher = factory(\App\Teacher::class)->times(1)->create();

        $classroom = factory(\App\Classroom::class)->times(1)->create();

        $student = \App\Student::create([
            'user_id' => $this->loggedInUser->id,
            'classroom_id' => $classroom[0]->id,
        ]);

        $category = factory(\App\Category::class)->times(1)->create();

        $operations = $student->operation()->saveMany(factory(\App\Operation::class)->times(2)->make());

        $response = $this->getJson("/api/operations?category={$category[0]->name}");

        $response->assertStatus(200)
            ->assertJson([
                'operations' => [
                    [
                        'id' => $operations[0]->id,
                        'category' => $operations[0]->category->name,
                        'quantity' => $operations[0]->quantity,
                        'image' => $operations[0]->image,
                        'author' => [
                            'username' => $this->loggedInUser->username,
                            'image' => $this->loggedInUser->image,
                            'classroom' => $operations[1]->student->classroom->name,
                        ]
                    ],
                    [
                        'id' => $operations[1]->id,
                        'category' => $operations[1]->category->name,
                        'quantity' => $operations[1]->quantity,
                        'image' => $operations[1]->image,
                        'author' => [
                            'username' => $this->loggedInUser->username,
                            'image' => $this->loggedInUser->image,
                            'classroom' => $operations[1]->student->classroom->name,
                        ]
                    ]
                ],
                'operationsCount' => 2
            ]);
    }
}
