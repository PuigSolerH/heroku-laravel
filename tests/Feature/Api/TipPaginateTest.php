<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TipPaginateTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_the_correct_tips_with_limit_and_offset()
    {
        $categories = factory(\App\Category::class)->times(2)->create();

        $this->user->tip()->saveMany(factory(\App\Tip::class)->times(25)->make());

        $response = $this->getJson('/api/tips');

        $response->assertStatus(200)
            ->assertJson([
                'tipsCount' => 25
            ]);

        $this->assertCount(20, $response->json()['tips'], 'Expected tips to set default limit to 20');

        $this->assertEquals(
            $this->user->tip()->latest()->take(20)->pluck('slug')->toArray(),
            array_column($response->json()['tips'], 'slug'),
            'Expected latest 20 tips by default'
        );

        $response = $this->getJson('/api/tips?limit=10&offset=5');

        $response->assertStatus(200)
            ->assertJson([
                'tipsCount' => 25
            ]);

        $this->assertCount(10, $response->json()['tips'], 'Expected tip limit of 10 when set');

        $this->assertEquals(
            $this->user->tip()->latest()->skip(5)->take(10)->pluck('slug')->toArray(),
            array_column($response->json()['tips'], 'slug'),
            'Expected latest 10 tips with 5 offset'
        );
    }
}
