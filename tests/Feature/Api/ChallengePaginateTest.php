<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ChallengePaginateTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_the_correct_challenges_with_limit_and_offset()
    {
        $teacher = \App\Teacher::create([
            'user_id' => $this->user->id,
        ]);

        $challenges = $teacher->challenge()
            ->saveMany(factory(\App\Challenge::class)->times(25)->make())
            ->each(function ($challenge) use ($teacher) {
                $challenge->teacher()->associate($teacher->id);
            });

        $response = $this->getJson('/api/challenges');

        $response->assertStatus(200)
            ->assertJson([
                'challengesCount' => 25
            ]);

        $this->assertCount(20, $response->json()['challenges'], 'Expected challenges to set default limit to 20');

        $this->assertEquals(
            $teacher->challenge()->latest()->take(20)->pluck('slug')->toArray(),
            array_column($response->json()['challenges'], 'slug'),
            'Expected latest 20 challenges by default'
        );

        $response = $this->getJson('/api/challenges?limit=10&offset=5');

        $response->assertStatus(200)
            ->assertJson([
                'challengesCount' => 25
            ]);

        $this->assertCount(10, $response->json()['challenges'], 'Expected challenge limit of 10 when set');

        $this->assertEquals(
            $teacher->challenge()->latest()->skip(5)->take(10)->pluck('slug')->toArray(),
            array_column($response->json()['challenges'], 'slug'),
            'Expected latest 10 challenges with 5 offset'
        );
    }
}
