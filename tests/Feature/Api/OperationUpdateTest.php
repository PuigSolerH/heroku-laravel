<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class OperationUpdateTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_the_updated_operation_on_successfully_updating_the_operation()
    {
        $users = factory(\App\User::class)->times(1)->create();

        $teacher = factory(\App\Teacher::class)->times(1)->create();

        $classroom = factory(\App\Classroom::class)->times(1)->create();

        $student = \App\Student::create([
            'user_id' => $this->loggedInUser->id,
            'classroom_id' => $classroom[0]->id,
        ]);

        $category = factory(\App\Category::class)->times(1)->create();

        $operation = $student->operation()->save(factory(\App\Operation::class)->make());

        $data = [
            'operation' => [
                'category_id' => $category[0]->id,
                'category' => $category[0]->name,
                'quantity' => '0',
            ]
        ];

        $response = $this->putJson("/api/operations/{$operation->id}", $data, $this->headers);

        $response->assertStatus(200)
            ->assertJson([
                'operation' => [
                    'id' => $response->baseResponse->original['operation']['id'],
                    'category' => $category[0]->name,
                    'quantity' => '0',
                    'author' => [
                        'username' => $this->loggedInUser->username,
                        'image' => $this->loggedInUser->image,
                        'classroom' => $classroom[0]->name,
                    ]
                ]
            ]);
    }

    /** @test */
    public function it_returns_appropriate_field_validation_errors_when_updating_the_operation_with_invalid_inputs()
    {
        $users = factory(\App\User::class)->times(1)->create();

        $teacher = factory(\App\Teacher::class)->times(1)->create();

        $classroom = factory(\App\Classroom::class)->times(1)->create();

        $student = \App\Student::create([
            'user_id' => $this->loggedInUser->id,
            'classroom_id' => $classroom[0]->id,
        ]);

        $category = factory(\App\Category::class)->times(1)->create();

        $operation = $student->operation()->save(factory(\App\Operation::class)->make());

        $data = [
            'operation' => [
                'category' => '',
                'quantity' => '',
            ]
        ];

        $response = $this->putJson("/api/operations/{$operation->id}", $data, $this->headers);

        $response->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'category' => ['must be a string.'],
                    'quantity' => ['must be a string.'],
                ]
            ]);
    }

    /** @test */
    public function it_returns_an_unauthorized_error_when_trying_to_update_operation_without_logging_in()
    {
        $users = factory(\App\User::class)->times(1)->create();

        $teacher = factory(\App\Teacher::class)->times(1)->create();

        $classroom = factory(\App\Classroom::class)->times(1)->create();

        $student = \App\Student::create([
            'user_id' => $this->loggedInUser->id,
            'classroom_id' => $classroom[0]->id,
        ]);

        $category = factory(\App\Category::class)->times(1)->create();

        $operation = $student->operation()->save(factory(\App\Operation::class)->make());

        $data = [
            'operation' => [
                'category' => 'new title',
                'quantity' => '0',
            ]
        ];

        $response = $this->putJson("/api/operations/{$operation->id}", $data);

        $response->assertStatus(401);
    }

    /** @test */
    public function it_returns_a_forbidden_error_when_trying_to_update_operations_by_others()
    {
        $users = factory(\App\User::class)->times(1)->create();

        $teacher = factory(\App\Teacher::class)->times(1)->create();

        $classroom = factory(\App\Classroom::class)->times(1)->create();

        $student = \App\Student::create([
            'user_id' => $this->user->id,
            'classroom_id' => $classroom[0]->id,
        ]);

        $category = factory(\App\Category::class)->times(1)->create();

        $operation = $student->operation()->save(factory(\App\Operation::class)->make());

        $data = [
            'operation' => [
                'category' => 'new title',
                'quantity' => '0',
            ]
        ];

        $response = $this->putJson("/api/operations/{$operation->id}", $data, $this->headers);

        $response->assertStatus(403);
    }
}
