<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class OperationPaginateTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_the_correct_challenges_with_limit_and_offset()
    {
        $users = factory(\App\User::class)->times(1)->create();

        $teacher = factory(\App\Teacher::class)->times(1)->create();

        $classroom = factory(\App\Classroom::class)->times(1)->create();

        $student = \App\Student::create([
            'user_id' => $this->loggedInUser->id,
            'classroom_id' => $classroom[0]->id,
        ]);

        $category = factory(\App\Category::class)->times(1)->create();

        $operations = $student->operation()->saveMany(factory(\App\Operation::class)->times(25)->make());

        $response = $this->getJson('/api/operations');

        $response->assertStatus(200)
            ->assertJson([
                'operationsCount' => 25
            ]);

        $this->assertCount(20, $response->json()['operations'], 'Expected operations to set default limit to 20');

        $this->assertEquals(
            $student->operation()->latest()->take(20)->pluck('id')->toArray(),
            array_column($response->json()['operations'], 'id'),
            'Expected latest 20 operations by default'
        );

        $response = $this->getJson('/api/operations?limit=10&offset=5');

        $response->assertStatus(200)
            ->assertJson([
                'operationsCount' => 25
            ]);

        $this->assertCount(10, $response->json()['operations'], 'Expected operation limit of 10 when set');

        $this->assertEquals(
            $student->operation()->latest()->skip(5)->take(10)->pluck('id')->toArray(),
            array_column($response->json()['operations'], 'id'),
            'Expected latest 10 operations with 5 offset'
        );
    }
}
