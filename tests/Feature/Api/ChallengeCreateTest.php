<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ChallengeCreateTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_the_challenge_on_successfully_creating_a_new_challenge()
    {
        $teacher = \App\Teacher::create([
            'user_id' => $this->loggedInUser->id,
        ]);

        $data = [
            'challenge' => [
                'title' => 'test title',
                'description' => 'test description',
                'teacher_id' => $teacher->id,
            ]
        ];

        $response = $this->postJson('/api/challenges', $data, $this->headers);

        $response->assertStatus(200)
            ->assertJson([
                'challenge' => [
                    'slug' => 'test-title',
                    'title' => 'test title',
                    'description' => 'test description',
                    'createdAt' => $response->baseResponse->original['challenge']['createdAt'],
                    'author' => [
                        'username' => $this->loggedInUser->username,
                        'image' => $this->loggedInUser->image,
                        'rol' => 'teacher',
                        'classroom' => ""
                    ]
                ]
            ]);
    }

    /** @test */
    public function it_returns_appropriate_field_validation_errors_when_creating_a_new_challenge_with_invalid_inputs()
    {
        $data = [
            'challenge' => [
                'title' => '',
                'description' => '',
            ]
        ];

        $response = $this->postJson('/api/challenges', $data, $this->headers);

        $response->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'title' => ['field is required.'],
                    'description' => ['field is required.'],
                ]
            ]);
    }

    /** @test */
    public function it_returns_an_unauthorized_error_when_trying_to_add_challenge_without_logging_in()
    {
        $response = $this->postJson('/api/challenges', []);

        $response->assertStatus(401);
    }
}
