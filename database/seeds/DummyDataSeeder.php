<?php

use Illuminate\Database\Seeder;

class DummyDataSeeder extends Seeder
{
    /**
     * Total number of users.
     *
     * @var int
     */
    protected $totalUsers = 20;

    /**
     * Total number of categories.
     *
     * @var int
     */
    protected $totalCategories = 5;

    /**
     * Total number of tips.
     *
     * @var int
     */
    protected $totalTips = 15;

    /**
     * Total number of teachers.
     *
     * @var int
     */
    protected $totalTeachers = 5;

    /**
     * Total number of challenges.
     *
     * @var int
     */
    protected $totalChallenges = 12;

    /**
     * Total number of students.
     *
     * @var int
     */
    protected $totalStudents = 10;

    /**
     * Total number of classrooms.
     *
     * @var int
     */
    protected $totalClassrooms = 5;

    /**
     * Total number of operations.
     *
     * @var int
     */
    protected $totalOperations = 15;


    /**
     * Populate the database with dummy data for testing.
     * Complete dummy data generation including relationships.
     * Set the property values as required before running database seeder.
     *
     * @param \Faker\Generator $faker
     */
    public function run(\Faker\Generator $faker)
    {
        $users = factory(\App\User::class)->times($this->totalUsers)->create();

        $categories = factory(\App\Category::class)->times($this->totalCategories)->create();

        $tips = factory(\App\Tip::class)->times($this->totalTips)->create();

        $teachers = factory(\App\Teacher::class)->times($this->totalTeachers)->create();

        $challenges = factory(\App\Challenge::class)->times($this->totalChallenges)->create();

        $classrooms = factory(\App\Classroom::class)->times($this->totalClassrooms)->create();

        $students = factory(\App\Student::class)->times($this->totalStudents)->create();

        $operations = factory(\App\Operation::class)->times($this->totalOperations)->create();
    }
}
