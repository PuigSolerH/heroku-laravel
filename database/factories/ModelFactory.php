<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (\Faker\Generator $faker) {

    return [
        'username' => str_replace('.', '', $faker->unique()->userName),
        'email' => $faker->unique()->safeEmail,
        'password' => 'secret',
        'bio' => $faker->sentence,
        'image' => 'https://cdn.worldvectorlogo.com/logos/laravel.svg',
    ];
});

$factory->define(App\Tip::class, function (\Faker\Generator $faker) {

    static $reduce = 999;
    static $category;
    $category = $category ?: \App\Category::all();
    static $user;
    $user = $user ?: \App\User::all();

    return [
        'image' => 'https://cdn.worldvectorlogo.com/logos/laravel.svg',
        'title' => $faker->sentence,
        'header' => $faker->sentence(10),
        'body' => $faker->paragraphs($faker->numberBetween(1, 3), true),
        'created_at' => \Carbon\Carbon::now()->subSeconds($reduce--),
        'category_id' => $category->random()->id,
        'user_id' => $user->random()->id,
    ];
});

$factory->define(App\Category::class, function (\Faker\Generator $faker) {

    return [
        'name' => $faker->unique()->word,
    ];
});

$factory->define(App\Challenge::class, function (\Faker\Generator $faker) {
    static $teacher;
    $teacher = $teacher ?: \App\Teacher::all();

    return [
        'title' => $faker->sentence,
        'description' => $faker->sentence(1, true),
        'teacher_id' => $teacher->random()->id,
    ];
});

$factory->define(App\Teacher::class, function (\Faker\Generator $faker) {
    static $user, $student;
    $user = \App\User::all()->pluck('id');
    $student = \App\Student::all()->pluck('user_id');

    $user = $user->toArray();
    $student = $student->toArray();

    $resultado = array_diff($user, $student);

    $unsignedUser = array_rand(array_flip($resultado));

    return [
        'user_id' => $unsignedUser,
    ];
});

$factory->define(App\Classroom::class, function (\Faker\Generator $faker) {
    static $teacher, $classroom;
    $teacher = \App\Teacher::all()->pluck('id');
    $classroom = \App\Classroom::all()->pluck('teacher_id');

    $teacher = $teacher->toArray();
    $classroom = $classroom->toArray();

    $resultado = array_diff($teacher, $classroom);

    $unsignedUser = array_rand(array_flip($resultado));

    return [
        'name' => $faker->unique()->word,
        'teacher_id' => $unsignedUser,
    ];
});

$factory->define(App\Student::class, function (\Faker\Generator $faker) {
    static $user, $classroom, $teacher;
    $user = \App\User::all()->pluck('id');
    $teacher = \App\Teacher::all()->pluck('user_id');
    $classroom = $classroom ?: \App\Classroom::all();

    $user = $user->toArray();
    $teacher = $teacher->toArray();

    $resultado = array_diff($user, $teacher);

    $unsignedUser = array_rand(array_flip($resultado));

    return [
        'user_id' => $unsignedUser,
        'classroom_id' => $classroom->random()->id,
    ];
});

$factory->define(App\Operation::class, function (\Faker\Generator $faker) {
    static $student, $category;
    $student = $student ?: \App\Student::all();
    $category = $category ?: \App\Category::all();

    return [
        'student_id' => $student->random()->id,
        'category_id' => $category->random()->id,
        'quantity' => $faker->randomNumber(2),
        'image' => 'https://cdn.worldvectorlogo.com/logos/laravel.svg',
    ];
});
