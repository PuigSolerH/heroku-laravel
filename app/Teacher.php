<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id'
    ];

    /**
     * Get the user that is associated to the student.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the classrooms that owns the teacher.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classroom()
    {
        return $this->hasMany(Classroom::class)->latest();
    }

    /**
     * Get all the challenges by the teacher.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function challenge()
    {
        return $this->hasMany(Challenge::class)->latest();
    }
}
