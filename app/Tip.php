<?php

namespace App;

use App\Utils\Slug\HasSlug;
use App\Utils\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;

class Tip extends Model
{
    use Filterable, HasSlug;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image',
        'title',
        'header',
        'body'
    ];

    /**
     * Get the list of categories attached to the tip.
     *
     * @return array
     */
    public function getCategoryListAttribute()
    {
        return Category::whereId($this['category_id'])->pluck("name")[0];
    }

    /**
     * Load all required relationships with only necessary content.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     */
    public function scopeLoadRelations($query)
    { }


    /**
     * Get all the tips that belong to the category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categories()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get all the tips that belong to the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * Get the key name for route model binding.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Get the attribute name to slugify.
     *
     * @return string
     */
    public function getSlugSourceColumn()
    {
        return 'title';
    }
}
