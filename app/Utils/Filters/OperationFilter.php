<?php

namespace App\Utils\Filters;

use App\User;
use App\Student;
use App\Category;
use App\Classroom;

class OperationFilter extends Filter
{
    /**
     * Filter by author username.
     * Get all the operations by the user with given username.
     *
     * @param $username
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function student($username)
    {
        $user = User::whereUsername($username)->first();

        if ($user) {
            $student = Student::where('user_id', $user->id)->first();
        } else {
            $student = null;
        }

        $studentId = $student ? $student->id : null;

        return $this->builder->where('student_id', $studentId);
    }

    /**
     * Filter by category name.
     * Get all the operations by the given category.
     *
     * @param $name
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function category($name)
    {
        $category = Category::whereName($name)->first();

        $categoriesIds = $category ? $category->id : null;

        return $this->builder->where('category_id', $categoriesIds);
    }

    /**
     * Filter by the classroom name.
     * Get all the operations by the given classroom.
     *
     * @param $classroom
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function classroom($classroom)
    {
        return $this->builder->Join('students', 'operations.student_id', 'students.id')
        ->Join('classrooms', 'students.classroom_id', 'classrooms.id')
        ->groupBy('classrooms.name')->having('classrooms.name', $classroom);
    }
}