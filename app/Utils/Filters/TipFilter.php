<?php

namespace App\Utils\Filters;

use App\Category;
use App\User;

class TipFilter extends Filter
{
    /**
     * Filter by author username.
     * Get all the tips by the user with given username.
     *
     * @param $username
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function author($username)
    {
        $user = User::whereUsername($username)->first();

        $userId = $user ? $user->id : null;

        return $this->builder->whereUserId($userId);
    }

    /**
     * Filter by category name.
     * Get all the tips tagged by the given category name.
     *
     * @param $name
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function category($name)
    {
        $category = Category::whereName($name)->first();

        $categoriesIds = $category ? $category->id : null;

        return $this->builder->where('category_id', $categoriesIds);
    }
}
