<?php

namespace App\Utils\Filters;

use App\User;
use App\Teacher;
use App\Classroom;
use App\Challenge;

class ChallengeFilter extends Filter
{
    /**
     * Filter by author username.
     * Get all the challenges by the user with given username.
     *
     * @param $username
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function author($username)
    {
        $user = User::whereUsername($username)->first();

        if ($user) {
            $teacher = Teacher::where('user_id', $user->id)->first();
        } else {
            $teacher = null;
        }

        $teacherId = $teacher ? $teacher->id : null;

        return $this->builder->where('teacher_id', $teacherId);
    }

    /**
     * Filter by classroom name.
     * Get the newest challenge filtered by the classroom name.
     *
     * @param $classroom
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function classroom($classroom)
    {
        $class = Classroom::whereName($classroom)->first();

        $teacherId = $class ? $class->teacher_id : null;

        $challengeNewest = Challenge::where('teacher_id', $teacherId)->orderBy('updated_at', 'DESC')->first();
        
        $challengeNewestId = $challengeNewest ? $challengeNewest->id : null;

        return $this->builder->whereId($challengeNewestId);
    }
}
