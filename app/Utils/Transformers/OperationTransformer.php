<?php

namespace App\Utils\Transformers;

class OperationTransformer extends Transformer
{
    /**
     * Resource name of the json object.
     *
     * @var string
     */
    protected $resourceName = 'operation';

    /**
     * Apply the transformation.
     *
     * @param $data
     * @return mixed
     */
    public function transform($data)
    {
        return [
            'id' => $data['id'],
            'category' => $data['category']['name'],
            'quantity' => $data['quantity'],
            'image' => $data['image'],
            'author' => [
                'username'  => $data['student']['user']['username'],
                'image'     => $data['student']['user']['image'],
                'classroom' => $data['student']['classroom']['name'],
            ]
        ];
    }
}