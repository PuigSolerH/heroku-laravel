<?php

namespace App\Utils\Transformers;

class UserTransformer extends Transformer
{
    /**
     * Resource name of the json object.
     *
     * @var string
     */
    protected $resourceName = 'user';

    /**
     * Apply the transformation.
     *
     * @param $data
     * @return mixed
     */
    public function transform($data)
    {
        $rol = 'student';
        $classroom = '';
        if (sizeof($data['teacher']) === 1) {
            $rol = 'teacher';
            if (sizeof($data['teacher'][0]['classroom']) === 1) {
                $classroom = $data['teacher'][0]['classroom'][0]['name'];
            }
        }

        if($rol === 'student') {
            $classroom = $data['student'][0]['classroom']['name'];
        }

        return [
            'email'     => $data['email'],
            'token'     => $data['token'],
            'username'  => $data['username'],
            'bio'       => $data['bio'],
            'image'     => $data['image'],
            'rol'       => $rol,
            'classroom' => $classroom,
        ];
    }
}
