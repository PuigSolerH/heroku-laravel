<?php

namespace App\Utils\Transformers;

class TipTransformer extends Transformer
{
    /**
     * Resource name of the json object.
     *
     * @var string
     */
    protected $resourceName = 'tips';

    /**
     * Apply the transformation.
     *
     * @param $data
     * @return mixed
     */
    public function transform($data)
    {
        return [
            'slug' => $data['slug'],
            'image' => $data['image'],
            'title' => $data['title'],
            'header' => $data['header'],
            'body' => $data['body'],
            'category' => $data['categoryList'],
            'createdAt' => $data['created_at']->toAtomString(),
            'author' => [
                'username'  => $data['user']['username'],
                'bio'       => $data['user']['bio'],
                'image'     => $data['user']['image'],
            ]
        ];
    }
}
