<?php

namespace App\Utils\Transformers;

class PasswordTransformer extends Transformer
{
    /**
     * Resource name of the json object.
     *
     * @var string
     */
    protected $resourceName = 'password';

    /**
     * Apply the transformation.
     *
     * @param $data
     * @return mixed
     */
    public function transform($data)
    {
        return [
            'email'     => $data['email'],
            'token'     => $data['token'],
        ];
    }
}
