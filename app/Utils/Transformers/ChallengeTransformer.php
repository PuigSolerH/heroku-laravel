<?php

namespace App\Utils\Transformers;

class ChallengeTransformer extends Transformer
{
    /**
     * Resource name of the json object.
     *
     * @var string
     */
    protected $resourceName = 'challenge';

    /**
     * Apply the transformation.
     *
     * @param $data
     * @return mixed
     */
    public function transform($data)
    {
        $rol = '';
        $classroom = '';
        if(!empty((array) $data['teacher'])) {
            $rol = 'teacher';
            if(!empty((array) $data['teacher']['classroom'])) {
                $classroom = $data['teacher']['classroom'][0]['name'];
            }
        }

        return [
            'slug' => $data['slug'],
            'title' => $data['title'],
            'description' => $data['description'],
            'createdAt' => $data['created_at']->toAtomString(),
            'updatedAt' => $data['updated_at']->toAtomString(),
            'author' => [
                'username'  => $data['teacher']['user']['username'],
                'image'     => $data['teacher']['user']['image'],
                'rol'       => $rol,
                'classroom' => $classroom,
            ]
        ];
    }
}
