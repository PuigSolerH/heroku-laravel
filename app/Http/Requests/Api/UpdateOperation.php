<?php

namespace App\Http\Requests\Api;

class UpdateOperation extends ApiRequest
{
    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        return $this->get('operation') ?: [];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $operation = $this->route('operation');

        return $operation->student->user_id == auth()->id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity' => 'sometimes|string|max:255',
            'category' => 'sometimes|string|max:255',
        ];
    }
}
