<?php

namespace App\Http\Requests\Api;

class DeleteOperation extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $operation = $this->route('operation');

        return $operation->student->user_id == auth()->id();
    }
}
