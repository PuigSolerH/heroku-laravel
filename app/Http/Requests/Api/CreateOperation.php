<?php

namespace App\Http\Requests\Api;

class CreateOperation extends ApiRequest
{
    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        return $this->get('operation') ?: [];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity' => 'required|string|max:255',
            'category' => 'required|string|max:255',
            'image' => 'sometimes|string',
        ];
    }
}
