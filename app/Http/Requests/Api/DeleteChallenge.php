<?php

namespace App\Http\Requests\Api;

class DeleteChallenge extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $challenge = $this->route('challenge');

        return $challenge->teacher->user_id == auth()->id();
    }
}
