<?php

namespace App\Http\Requests\Api;

class Contact extends ApiRequest
{
    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        return $this->get('data') ?: [];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|max:255',
            'subject' => 'required|string|max:255',
            'comment' => 'required|string'
        ];
    }
}
