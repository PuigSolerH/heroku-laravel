<?php

namespace App\Http\Controllers\Api;

use App\Classroom;
use App\Utils\Transformers\ClassroomTransformer;

class ClassroomController extends ApiController
{

    /**
     * ClassroomController constructor.
     *
     * @param ClassroomTransformer $transformer
     */
    public function __construct(ClassroomTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * @SWG\Get(
     *      path="categories",
     *      tags={"Categories"},
     *      summary="Get Categories",
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function index()
    {
        $classrooms = Classroom::all()->pluck('name');

        return $this->respondWithTransformer($classrooms);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
