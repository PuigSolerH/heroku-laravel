<?php

namespace App\Http\Controllers\Api;

use App\Tip;
use App\Utils\Paginate\Paginate;
use App\Utils\Filters\TipFilter;
use App\Utils\Transformers\TipTransformer;

class TipController extends ApiController
{
    /**
     * TipController constructor.
     *
     * @param TipTransformer $transformer
     */
    public function __construct(TipTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @param TipFilter $filter
     * @return \Illuminate\Http\Response
     * 
     * @SWG\Get(
     *      path="tips",
     *      tags={"Tips"},
     *      summary="Get Tips",
     *      @SWG\Parameter(
     *         name="category",
     *         in="query",
     *         type="string",
     *         description="Category",
     *         required=false,
     *     ),
     *     @SWG\Parameter(
     *         name="author",
     *         in="query",
     *         type="string",
     *         description="Author",
     *         required=false,
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @SWG\Response(
     *         response=422,
     *         description="Missing Data"
     *     )
     * )
     */
    public function index(TipFilter $filter)
    {
        $tips = new Paginate(Tip::loadRelations()->filter($filter));

        return $this->respondWithPagination($tips);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Tip $tips
     * @return \Illuminate\Http\Response
     * 
     * @SWG\Get(
     *      path="tips/{slug}",
     *      tags={"Tips"},
     *      summary="Get the specified Tip",
     *     @SWG\Parameter(
     *         name="slug",
     *         in="path",
     *         type="string",
     *         description="Slug",
     *         required=true,
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @SWG\Response(
     *         response=422,
     *         description="Missing Data"
     *     )
     * )
     */
    public function show(Tip $tips)
    {
        return $this->respondWithTransformer($tips);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
