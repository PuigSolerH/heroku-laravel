<?php

namespace App\Http\Controllers\Api;

use App\Operation;
use App\Student;
use App\Category;
use App\Utils\Paginate\Paginate;
use App\Utils\Filters\OperationFilter;
use App\Http\Requests\Api\CreateOperation;
use App\Http\Requests\Api\UpdateOperation;
use App\Http\Requests\Api\DeleteOperation;
use App\Utils\Transformers\OperationTransformer;
use Illuminate\Support\Facades\DB;

class OperationController extends ApiController
{
    /**
     * OperationController constructor.
     *
     * @param OperationTransformer $transformer
     */
    public function __construct(OperationTransformer $transformer)
    {
        $this->transformer = $transformer;

        $this->middleware('auth.api')->except(['index', 'indexAmount', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param OperationFilter $filter
     * @return \Illuminate\Http\Response
     * 
     * @SWG\Get(
     *      path="operations",
     *      tags={"Operations"},
     *      summary="Get Operations",
     *     @SWG\Parameter(
     *         name="student",
     *         in="query",
     *         type="string",
     *         description="Student",
     *         required=false,
     *     ),
     *      @SWG\Parameter(
     *         name="category",
     *         in="query",
     *         type="string",
     *         description="Category",
     *         required=false,
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @SWG\Response(
     *         response=422,
     *         description="Missing Data"
     *     )
     * )
     */
    public function index(OperationFilter $filter)
    {
        $operations = new Paginate(Operation::loadRelations()->filter($filter));

        return $this->respondWithPagination($operations);
    }

    /**
     * Display a listing of the resource.
     *
     * @SWG\Get(
     *      path="operationsAmount",
     *      tags={"Operations"},
     *      summary="Get Operations amount",
     *     @SWG\Parameter(
     *         name="student",
     *         in="query",
     *         type="string",
     *         description="Student",
     *         required=false,
     *     ),
     *      @SWG\Parameter(
     *         name="classroom",
     *         in="query",
     *         type="string",
     *         description="Classroom",
     *         required=false,
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @SWG\Response(
     *         response=422,
     *         description="Missing Data"
     *     )
     * )
     * @param OperationFilter $filter
     * @return \Illuminate\Http\Response
     */
    public function indexAmount(OperationFilter $filter)
    {    
        $operationsAmount = Operation::select('categories.name', DB::raw('CASE WHEN SUM(operations.quantity) IS NULL THEN 0 ELSE SUM(operations.quantity) END'))
        ->rightJoin('categories', 'operations.category_id', 'categories.id')
        ->groupBy('categories.name')->filter($filter)
        ->get();

        return $this->respond($operationsAmount);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateOperation $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOperation $request)
    {
        $student = Student::where('user_id', auth()->id())->first();
        
        $category = Category::whereName($request->input('operation.category'))->first();

        if ($student) {
            $operation = $student->operation()->create([
                'category_id' => $category->id,
                'quantity' => $request->input('operation.quantity'),
                'student_id' => $student->id,
            ]);

            return $this->respondWithTransformer($operation);
        } else {
            return null;
        }
    }

    /**
     * Display the specified resource.
     *
     * @SWG\Get(
     *      path="operations/{id}",
     *      tags={"Operations"},
     *      summary="Get the specified Operation",
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         type="integer",
     *         description="Id",
     *         required=true,
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @SWG\Response(
     *         response=422,
     *         description="Missing Data"
     *     )
     * )
     * @param Operation $request
     * @return \Illuminate\Http\Response
     */
    public function show(Operation $operation)
    {
        return $this->respondWithTransformer($operation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateOperation $request
     * @param Operation $operation
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOperation $request, Operation $operation)
    {
        if ($request->has('operation')) {
            $category = Category::whereName($request->get('operation')['category'])->first();

            $operation->update(
                [
                    'category_id' => $category->id, 
                    'quantity' => $request->get('operation')['quantity']
                ]
            );
        }

        return $this->respondWithTransformer($operation);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DeleteOperation $request
     * @param Operation $operation
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteOperation $request, Operation $operation)
    {
        $operation->delete();

        return $this->respondSuccess();
    }
}
