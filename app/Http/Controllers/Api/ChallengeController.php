<?php

namespace App\Http\Controllers\Api;

use App\Challenge;
use App\Teacher;
use App\Utils\Paginate\Paginate;
use App\Utils\Filters\ChallengeFilter;
use App\Http\Requests\Api\CreateChallenge;
use App\Http\Requests\Api\UpdateChallenge;
use App\Http\Requests\Api\DeleteChallenge;
use App\Utils\Transformers\ChallengeTransformer;

class ChallengeController extends ApiController
{
    /**
     * ChallengeController constructor.
     *
     * @param ChallengeTransformer $transformer
     */
    public function __construct(ChallengeTransformer $transformer)
    {
        $this->transformer = $transformer;

        $this->middleware('auth.api')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param ChallengeFilter $filter
     * @return \Illuminate\Http\Response
     * 
     * @SWG\Get(
     *      path="challenges",
     *      tags={"Challenges"},
     *      summary="Get Challenges",
     *     @SWG\Parameter(
     *         name="author",
     *         in="query",
     *         type="string",
     *         description="Author",
     *         required=false,
     *     ),
     *     @SWG\Parameter(
     *         name="classroom",
     *         in="query",
     *         type="string",
     *         description="Classroom",
     *         required=false,
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @SWG\Response(
     *         response=422,
     *         description="Missing Data"
     *     )
     * )
     */
    public function index(ChallengeFilter $filter)
    {
        $challenges = new Paginate(Challenge::loadRelations()->filter($filter));

        return $this->respondWithPagination($challenges);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateChallenge $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateChallenge $request)
    {
        $user = auth()->user();

        $teacher = Teacher::where('user_id', auth()->id())->first();

        if ($teacher) {
            $challenge = $teacher->challenge()->create([
                'title' => $request->input('challenge.title'),
                'description' => $request->input('challenge.description'),
                'teacher_id' => $teacher->id,
            ]);

            return $this->respondWithTransformer($challenge);
        } else {
            return null;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param CreateChallenge $request
     * @return \Illuminate\Http\Response
     * 
     * @SWG\Get(
     *      path="challenges/{slug}",
     *      tags={"Challenges"},
     *      summary="Get the specified Challenge",
     *     @SWG\Parameter(
     *         name="slug",
     *         in="path",
     *         type="string",
     *         description="Slug",
     *         required=true,
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @SWG\Response(
     *         response=422,
     *         description="Missing Data"
     *     )
     * )
     */
    public function show(Challenge $challenge)
    {
        return $this->respondWithTransformer($challenge);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateChallenge $request
     * @param Challenge $challenge
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateChallenge $request, Challenge $challenge)
    {
        if ($request->has('challenge')) {
            $challenge->update($request->get('challenge'));
        }

        return $this->respondWithTransformer($challenge);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DeleteChallenge $request
     * @param Challenge $challenge
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteChallenge $request, Challenge $challenge)
    {
        $challenge->delete();

        return $this->respondSuccess();
    }
}
