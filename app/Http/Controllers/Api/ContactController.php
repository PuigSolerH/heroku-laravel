<?php
namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Contact;

use Mail;

class ContactController extends ApiController
{
    /**
     * @param Contact $request
     * @return \Illuminate\Http\Response
     * 
     * @SWG\Post(
     *     path="contact",
     *     tags={"Contact"},
     *     operationId="api.contact.contact",
     *     summary="Send a contact Mail",
     *     consumes={"multipart/form-data"},
     *     produces={"text/plain, application/json"},
     *      @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          required=true, 
     *          type="string" 
     *      ),
     *      @SWG\Parameter(
     *          name="subject",
     *          in="formData",
     *          required=true, 
     *          type="string" 
     *      ),
     *      @SWG\Parameter(
     *          name="comment",
     *          in="formData",
     *          required=true, 
     *          type="string" 
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function contact(Contact $request)
    {
        $data = array(
            'email' => $request->input('data.email'),
            'subject' => $request->input('data.subject'),
            'comment' => $request->input('data.comment'),
        );

        $sent = Mail::send('email.email', $data, function ($message) use ($data) {
            $message->subject('Test email');
            $message->from($data['email']);
            $message->to('hiber98@gmail.com');
        });
        if ($sent) dd("something wrong");

        return response()->json(['message' => 'Request completed']);
    }
}
