<?php

namespace App\Http\Controllers\Api;

use Auth;
use Mail;
use App\User;
use App\Student;
use App\Classroom;
use App\PasswordResets;
use App\Http\Requests\Api\LoginUser;
use App\Http\Requests\Api\RegisterUser;
use App\Http\Requests\Api\Verify;
use App\Http\Requests\Api\Token;
use App\Utils\Transformers\UserTransformer;
use App\Utils\Transformers\PasswordTransformer;

class AuthController extends ApiController
{
    /**
     * AuthController constructor.
     *
     * @param UserTransformer $transformer
     */
    public function __construct(UserTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * Login user and return the user if successful.
     *
     * @param LoginUser $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginUser $request)
    {
        $credentials = $request->only('user.email', 'user.password');
        $credentials = $credentials['user'];

        if (!Auth::once($credentials)) {
            return $this->respondFailedLogin();
        }

        return $this->respondWithTransformer(auth()->user());
    }

    /**
     * Register a new user and return the user if successful.
     *
     * @param RegisterUser $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterUser $request)
    {
        $user = User::where('email', $request->input('user.email'))->first();
        if ($user) {
            return;
        } else {
            $image = 'https://ui-avatars.com/api/?size=50&name=' . $request->input("user.username");

            $user = User::create([
                'username' => $request->input('user.username'),
                'email' => $request->input('user.email'),
                'password' => $request->input('user.password'),
                'image' => $image,
            ]);

            if ($user->id) {
                $classroom = Classroom::whereName($request->input("user.classroom"))->first();

                Student::create([
                    'user_id' => $user->id,
                    'classroom_id' => $classroom->id,
                ]);
            }

            return $this->respondWithTransformer($user);
        }
    }
}
