<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {

    Route::post('users/login', 'AuthController@login');
    Route::post('users', 'AuthController@register');

    Route::get('user', 'UserController@index');
    Route::match(['put', 'patch'], 'user', 'UserController@update');

    Route::post('contact', 'ContactController@contact');

    Route::resource('categories', 'CategoryController', [
        'except' => [
            'create', 'store', 'show', 'edit', 'update', 'destroy'
        ]
    ]);

    Route::resource('classrooms', 'ClassroomController', [
        'except' => [
            'create', 'store', 'show', 'edit', 'update', 'destroy'
        ]
    ]);

    Route::resource('challenges', 'ChallengeController', [
        'except' => [
            'create', 'edit'
        ]
    ]);

    Route::resource('tips', 'TipController', [
        'except' => [
            'create', 'store', 'edit', 'update', 'destroy'
        ]
    ]);


    Route::get('operationsAmount', 'OperationController@indexAmount');
    
    Route::resource('operations', 'OperationController', [
        'except' => [
            'create', 'edit'
        ]
    ]);
});
